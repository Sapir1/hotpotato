"""
Server unit test functions.
"""


import factory

from hotpotato import models
from hotpotato.api.server import key as api_server_key
from hotpotato.models import Server, db


class ServerFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Server
        sqlalchemy_session = db.session

    hostname = factory.LazyFunction(
        lambda: "{}-prod-monitor{}".format(
            factory.Faker("word").generate({}),
            factory.Faker("random_element", elements=[1, 2, 3]).generate({}),
        )
    )
    apikey = factory.LazyFunction(api_server_key.create)
    enable_heartbeat = factory.Faker("boolean")
    disable_missed_heartbeat_notif = factory.Faker("boolean")
    missed_heartbeat_limit = factory.Faker("random_int", max=20)
    timezone = factory.Faker("timezone")
    link = factory.Faker("url")


def create_fake(fake):
    """
    Generate a fake server, with randomly set data.
    """

    return models.Server(
        hostname="{}-prod-monitor{}".format(
            fake.word(), fake.random_element([1, 2, 3])
        ),
        apikey=api_server_key.create(),
        enable_heartbeat=fake.boolean(),
        disable_missed_heartbeat_notif=fake.boolean(),
        missed_heartbeat_limit=fake.random.randrange(20),
        timezone=fake.timezone(),
        link=fake.url(),
    )
