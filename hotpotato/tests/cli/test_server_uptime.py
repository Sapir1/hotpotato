def test_update_avail_last_1d_7d(runner, session):
    """
    Test update-avail-last-1d-7d runs without any errors.
    """
    result = runner.invoke(args=["server-uptime", "update-avail-last-1d-7d"])
    assert result.exit_code is 0


def test_update_avail_last_month(runner, session):
    """
    Test update-avail-last-month runs without any errors.
    """
    result = runner.invoke(args=["server-uptime", "update-avail-last-month"])
    assert result.exit_code is 0
