"""
Heartbeat model classes.
"""
from hotpotato.models.database import db
from hotpotato.models.servers import Server


class Heartbeat(db.Model):
    """
    Stores times that monitoring servers reported in to confirm their availability.
    """

    __tablename__ = "heartbeats"

    id = db.Column(db.Integer, primary_key=True)
    server_id = db.Column(db.Integer, db.ForeignKey(Server.id, ondelete="CASCADE"))
    received_dt = db.Column(db.DateTime)
