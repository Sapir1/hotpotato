from hotpotato.models.database import db
from hotpotato.modes.users import User

#
# Deprecated models.
# TODO: Remove deprecated models in 0.8.5
#


# The different methods we can contact people, so we're not storing a bunch of text
# TODO: Simplify oncall contact methods so that it uses Enums and just a single table.
class oncall_contact_methods(db.Model):  # noqa: N801
    """
    Stores the various methods of contacting on-call people.
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)  # "sms", "voice", "signal", etc
    enabled = db.Column(db.Boolean)


class oncall_contact_prefs(db.Model):  # noqa: N801
    """
    Stores contact preferences for on-call people.
    Relates to the oncall_contact_methods table, stores arbitrary strings
    such as phone numbers, user names, etc.
    """

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)  # Relationship
    contact = db.Column(db.Text)  # The phone number or user ID to contact
    method = db.Column(db.Integer, db.ForeignKey("oncall_contact_methods.id"))
    methods = db.relationship("oncall_contact_methods")  # Relationship
    send_pages = db.Column(db.Boolean)
    send_failures = db.Column(db.Boolean)


# TODO: notify_on_failure is deprecated, remove in 0.8.5
class notify_on_failure(db.Model):  # noqa: N801
    """
    Stores details of who to contact in the event of a failure,
    in addition to the on-call person.
    """

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)


# TODO: notifications is deprecated, remove in 0.8.5
class notifications(db.Model):  # noqa: N801
    """
    Stores notifications that are sent to Hot Potato.
    """

    id = db.Column(db.Integer, primary_key=True)
    # Things from Icinga
    hostname = db.Column(db.Text)
    servicename = db.Column(db.Text, nullable=True)
    troublecode = db.Column(db.Text, nullable=True)
    displayname = db.Column(db.Text, nullable=True)
    output = db.Column(db.Text)
    notificationtype = db.Column(db.Text)
    state = db.Column(db.Text)
    icingadatetime = db.Column(db.DateTime)
    notification_author = db.Column(db.Text, nullable=True)
    notification_comment = db.Column(db.Text, nullable=True)
    source_id = db.Column(db.Integer, db.ForeignKey("servers.id"), nullable=True)
    source = db.relationship("servers")  # Relationship
    acked = db.Column(db.Boolean, default=0)
    datetimeinserted = db.Column(db.DateTime)
    # Notification things -> To be migrated to the modicaoutbound table.
    notified = db.Column(db.Boolean, default=0)
    modica_id = db.Column(db.Integer, nullable=True)


# TODO: modicaoutbound is deprecated, remove in 0.8.5
class modicaoutbound(db.Model):  # noqa: N801
    """
    Stores details of outbound messages sent via Modica.
    """

    id = db.Column(db.Integer, primary_key=True)
    sending_dt = db.Column(db.DateTime)
    message_content = db.Column(db.Text, nullable=True)
    destination = db.Column(db.Text, nullable=True)
    # The ID number the Modica API gives us back
    modica_id = db.Column(db.Integer)
    notification_id = db.Column(db.Integer)
    custom_id = db.Column(db.Integer)
    handover_id = db.Column(db.Integer)


# TODO: modicastatus is deprecated, remove in 0.8.5
class modicastatus(db.Model):  # noqa: N801
    """
    Sends status information of messages sent via Modica.
    """

    id = db.Column(db.Integer, primary_key=True)
    outboundid = db.Column(db.Integer)
    statustime = db.Column(db.DateTime)
    status = db.Column(
        db.Text
    )  # Can be: submitted, sent, received, frozen, rejected, failed, dead and expired.


# TODO: modicainbound is deprecated, remove in 0.8.5
class modicainbound(db.Model):  # noqa: N801
    """
    Stores incoming messages from Modica.
    """

    id = db.Column(db.Integer, primary_key=True)
    received_dt = db.Column(db.DateTime)
    # Modica MOM details
    content = db.Column(db.Text, nullable=True)
    operator = db.Column(db.Text, nullable=True)
    destination = db.Column(db.Text, nullable=True)
    source = db.Column(
        db.Text, nullable=True
    )  # We can look this up in the users information eventually
    modica_id = db.Column(db.Integer, nullable=True)  # Link to the modicaoutbound table
    reply_to_id = db.Column(db.Integer, nullable=True)


# TODO: handovers is deprecated, remove in 0.8.5
class handovers(db.Model):  # noqa: N801
    """
    Stores handover details.
    """

    id = db.Column(db.Integer, primary_key=True)
    rotation = db.Column(db.Integer, db.ForeignKey("rotations.id"))
    changetime = db.Column(db.DateTime)
    hofrom = db.Column(db.Integer, db.ForeignKey(User.id))
    hoto = db.Column(db.Integer, db.ForeignKey(User.id))
    message = db.Column(db.Text)


# TODO: messages is deprecated, remove in 0.8.5
class messages(db.Model):  # noqa: N801
    """
    Stores custom messages that are sent via Hot Potato.
    """

    id = db.Column(db.Integer, primary_key=True)
    rotation = db.Column(db.Integer, db.ForeignKey("rotations.id"))
    time = db.Column(db.DateTime)
    mfrom = db.Column(db.Integer, db.ForeignKey(User.id))
    mto = db.Column(db.Integer, db.ForeignKey(User.id))
    message = db.Column(db.Text)


# TODO: twilio_outbound_sms is deprecated, remove in 0.8.5
class twilio_outbound_sms(db.Model):  # noqa: N801
    """
    Stores outbound messages to Twilio.
    """

    id = db.Column(db.Integer, primary_key=True)
    sending_dt = db.Column(db.DateTime)
    message_content = db.Column(db.Text, nullable=True)
    destination = db.Column(db.Text, nullable=True)
    notification_id = db.Column(db.Integer)  # Future use
    custom_id = db.Column(db.Integer)  # Future use
    handover_id = db.Column(db.Integer)  # Future use
    twilio_ref = db.Column(db.Text, nullable=True)
