"""
Server API version 2 endpoints for heartbeats.
"""


from http import HTTPStatus

from hotpotato import heartbeats
from hotpotato.api.server.v2 import decorators, functions as api_server_functions
from hotpotato.api.server.v2._blueprint import blueprint


@blueprint.route("/heartbeats/create", methods=["POST"])
@decorators.auth_required
def heartbeats_create():
    """
    Create a heartbeat for a server.
    """

    server = api_server_functions.server_get_from_auth()
    heartbeat = heartbeats.create(server.id)

    return api_server_functions.api_json_response_get(
        code=HTTPStatus.CREATED,
        no_cache=True,
        data={
            "id": heartbeat.id,
            "server_id": heartbeat.server_id,
            "received_dt": heartbeat.received_dt,
        },
    )
