"""
Server API endpoints for Twilio callbacks.
"""


import flask
from flask import current_app
from webargs import fields
from webargs.flaskparser import parser

from hotpotato import models
from hotpotato.notifications import (
    exceptions as notifications_exceptions,
    notifications,
)

# TODO: Handle incoming messages in Twilio
#       https://www.twilio.com/docs/sms/tutorials/how-to-receive-and-reply-python


class MessageStatusError(Exception):
    """
    Message status exception class.
    """

    pass


notif_args = {
    "MessageSid": fields.Str(required=True),
    "MessageStatus": fields.Str(required=True),
    "ErrorCode": fields.Str(),
    "ErrorMessage": fields.Str(),
}


def notificationstatus():
    """
    Handle delivery status receipts from Twilio.
    """

    json_obj = parser.parse(notif_args, flask.request)

    provider_notif_id = json_obj["MessageSid"]
    provider_notif_status = json_obj["MessageStatus"]

    current_app.logger.debug("Received delivery receipt from Twilio:")
    current_app.logger.debug("- provider_notif_id = {}".format(provider_notif_id))
    current_app.logger.debug(
        "- provider_notif_status = {}".format(provider_notif_status)
    )

    # Try to find the notification associated with the provider notification ID.
    try:
        notif = notifications.get_by_provider_notif_id("twilio", provider_notif_id)
    except notifications_exceptions.NotificationProviderNotificationIDError as err:
        err_message = "While receiving message status from Twilio: {}".format(str(err))
        current_app.logger.error(err_message)
        return (err_message, 400)

    current_app.logger.debug(
        "Found associated notification with ID: {}".format(notif.id)
    )

    # Modify the status of the message based on the information in the delivery receipt.
    if (
        provider_notif_status == "accepted"
        or provider_notif_status == "queued"
        or provider_notif_status == "sending"
    ):
        notif.json["status"] = "RECEIVED_BY_PROVIDER"
    elif provider_notif_status == "sent":
        notif.json["status"] = "SENT_TO_CLIENT"
    elif provider_notif_status == "delivered":
        notif.json["status"] = "RECEIVED_BY_CLIENT"
    elif provider_notif_status == "undelivered" or provider_notif_status == "failed":
        notif.json["status"] = "SEND_FAILED"
        notif.json["errors"].append(
            "Twilio: Error {}: {}".format(
                json_obj.get("ErrorCode", None), json_obj.get("ErrorMessage", None)
            )
        )
    else:
        body = "Unknown message status value received from Twilio: {}".format(
            provider_notif_status
        )
        notif.json["errors"].append(body)
        raise MessageStatusError(body)

    current_app.logger.debug(
        "Changed status of notification with ID {} to {}".format(
            notif.id, notif.json["status"]
        )
    )

    if notif.json["status"] == "SEND_FAILED":
        current_app.logger.error(
            "Failed to send notification with ID {}: {}".format(
                notif.id, notif.json["errors"][-1]
            )
        )

    models.db.session.commit()

    return ("", 204)
