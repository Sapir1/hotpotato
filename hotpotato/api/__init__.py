"""
API classes and helper functions.
"""


from hotpotato.api.app import v1 as api_app_v1
from hotpotato.api.server import v1 as api_server_v1, v2 as api_server_v2
from hotpotato.api.stats import v1 as api_stats_v1
from hotpotato.api.web import v1 as api_web_v1


def init_app(app):
    """
    Initialise the given app with the API blueprints.
    """

    app.register_blueprint(api_app_v1.blueprint, url_prefix="/api/app/v1")

    # TODO: Accessing the version 1 API via /api/v1 is deprecated,
    #       remove this in <undetermined future version>.
    app.register_blueprint(api_server_v1.blueprint, url_prefix="/api/v1")
    app.register_blueprint(api_server_v1.blueprint, url_prefix="/api/server/v1")
    app.register_blueprint(api_server_v2.blueprint, url_prefix="/api/server/v2")

    app.register_blueprint(api_web_v1.blueprint, url_prefix="/api/web/v1")
    app.register_blueprint(api_stats_v1.blueprint, url_prefix="/api/stats/v1")
