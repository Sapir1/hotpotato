"""
Stats API version 1 blueprint and endpoints.
"""


from hotpotato.api.stats.v1 import notifications  # noqa: F401
from hotpotato.api.stats.v1._blueprint import blueprint  # noqa: F401
