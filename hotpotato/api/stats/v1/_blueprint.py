"""
Stats API version 1 blueprint.
"""


import flask

blueprint = flask.Blueprint("api_stats_v1", __name__)
