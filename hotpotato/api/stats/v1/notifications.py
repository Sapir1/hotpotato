import datetime
import json
from datetime import timezone

import pytz

from hotpotato.api.stats.v1._blueprint import blueprint
from hotpotato.models import db
from hotpotato.models.notifications.notifications import Notification
from hotpotato.util import datetime_process


@blueprint.route("/last_alert")
def alert():

    notif = db.session.query(Notification).order_by(Notification.received_dt).first()

    date = datetime_process(notif.received_dt, as_utc=True, to_user_tz=True)

    eastern = pytz.timezone("Pacific/Auckland")
    current = datetime.datetime.now(timezone.utc).astimezone(eastern)

    s = (current - date).seconds
    days, remainder = divmod(s, 86400)
    hours, mins = divmod(remainder, 3600)
    minutes, seconds = divmod(mins, 60)

    diff = {"days": days, "hours": hours, "minutes": mins, "seconds": seconds}

    json_data = {"last_alert": str(date), "time_since_last_alert": diff}
    return json.dumps(json_data)
