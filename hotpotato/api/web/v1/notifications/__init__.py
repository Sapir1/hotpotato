"""
Web API version 1 notification endpoints.
"""


from hotpotato.api.web.v1.notifications import get  # noqa: F401
