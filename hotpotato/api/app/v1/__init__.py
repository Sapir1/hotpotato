"""
App API version 1 blueprint and endpoints.
"""


from hotpotato.api.app.v1._blueprint import blueprint  # noqa: F401
