"""
Hot Potato notifications queue worker app. This is started using the Dramatiq command line tool.
"""


from hotpotato import app as hotpotato_app

app = hotpotato_app.create(init_cli=False, setup_server_name=True)
